import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

import data from '../data/data.json'

import imgBlue from '../images/marker-blue.png'
import imgRed from '../images/marker-red.png'

class Place extends React.Component {
  render(){
    return (
      <img src={this.props.cor}></img>
    )
  }
}
 
class Map extends Component {
  constructor(){
    super()
    this.state = {
      data:data.stores, 
      places: [], 
      revenue: 15000
    }
  }
  
  static defaultProps = {
    center: {
      lat: -23.568767,
      lng: -46.649907, 
    },
    zoom: 11,
  }
 
  componentWillMount(){
    let places= []
    for (const store of this.state.data){
       places.push({latitude:store.latitude, longitude:store.longitude, revenue:store.revenue, name:store.name})
    }
     this.setState({places: places})
  }

   componentWillReceiveProps() {
    this.setState({places: this.props.onPlacesChange})      
  }

 
 
  render() {
   
    return (
      <div style={{ height: '500px', width: '500px' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyDL3CoVHObq609VDybODRBDqwG14De03fQ' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          className="map"
        >

          {this.state.places.map(item=>{
            if(item.revenue < this.props.onRevenueChange){
            return <Place  cor={imgRed} lat={item.latitude} lng={item.longitude} />
            }
            return <Place cor={imgBlue} lat={item.latitude} lng={item.longitude} />
          })}
            
        </GoogleMapReact>
       

      </div>
    );
  }
}
 
export default Map;