import React from 'react'

import './MainContent.css'

import data from '../data/data.json'

import Map from './Maps.js'
import Input from './Input.js'




 class MainContent extends React.Component {

  constructor(){
    super()
    this.state = {
      data:data.stores, 
      places: [], 
      revenue: 15000, 
      page: 'closed'
    }
  }
 
  componentWillMount(){
    let places= []
    for (const store of this.state.data){
       places.push({name:store.name, revenue:store.revenue, latitude: store.latitude, longitude: store.longitude})
    }
     this.setState({places: places})
  }

  handleInputSearch = (e) => {
    const inputValue = e.target.value.toLowerCase()
    let places=[]
    this.state.data.filter((item)=>{
      if(item.name.toLowerCase().includes(inputValue)){
        places.push({name:item.name, revenue:item.revenue, latitude: item.latitude, longitude: item.longitude})
        this.setState({places:places})
      }
    })
  }
  
  handleInputRevenue =(e) =>{
      this.setState({revenue:e.target.value})
      return e.target.value
  }

  render(){
    return(
      <React.Fragment>

            <Input value={this.handleInputRevenue} handleInputSearch={this.handleInputSearch}  handleInputRevenue={this.handleInputRevenue} />
            <div className='container'>  
              <ul>
                <li> Loja <span className="lista__item">Faturamento</span></li>
                {this.state.places.map((item, index) =>{
                  if(this.state.revenue === 0 || this.state.revenue === ""){
                    this.setState({revenue: 15000})
                  }
                  if(index >= 10){return}
                  if(item.revenue < this.state.revenue){
                    return <li className="lista__item">Loja {index+1} <span className="lista__item red">R$ {String(item.revenue).replace(".", ",")}</span></li>
                  }else{
                    return <li className="lista__item">Loja {index+1} <span className="lista__item">R$ {String(item.revenue).replace(".", ",")}</span></li>
                }})}
              </ul>
              <Map className="mapa" app_id="GmYWFK7v4aKRpXWuN8sL" app_code="xUF__yTapkzCPf2gJ58a8Q" onPlacesChange={this.state.places} onRevenueChange={this.state.revenue} />
            </div>

        </React.Fragment>
          
    )
  }
}

export default MainContent



