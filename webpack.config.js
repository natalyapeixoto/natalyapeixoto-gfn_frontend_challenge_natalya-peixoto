const path = require('path');
 const HWP = require('html-webpack-plugin');
 module.exports = {
     entry: path.join(__dirname, '/src/index.js'),    output: {
        filename: 'build.js',
       path: path.join(__dirname, '/dist')},
    module:{
        rules:[{
           test: /\.js$/,
           exclude: /node_modules/,
           loader: 'babel-loader',
           exclude: /node_modules/
           }, 
           {test: /\.css$/, use: [ 'style-loader', 'css-loader' ]
        }, {
            test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
            loader: 'url-loader?limit=100000' }]
        },
    devServer:{
        open: 'Google Chrome'
    },
    plugins:[
  new HWP(
               {template: path.join(__dirname,'/src/index.html')}
            )
        ]
    }