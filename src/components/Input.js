import React from 'react'

class Input extends React.Component {

  render(){
    return(
        <div className="container">
          <div className="search">
           <input onChange={this.props.handleInputSearch}  className='search__input' type="text" placeholder='Pesquisa' />
           <Search />
          </div>
          <div className='faturamento'>
            <label className='label'>Faturamento mínimo esperado</label>
            <input onKeyUpCapture={this.props.handleInputRevenue}  type='number' placeholder="15.000,00" className='faturamento__input' />
          </div>
        </div>
    )
  }
}

export default Input

class Search extends React.Component {
  render(){
    return (

      <svg
      className='search__svg'
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      fill="#B6B9B9"
      preserveAspectRatio="xMidYMid meet"
      viewBox="0 0 20 20"
      height="25"
      width="25">
     <path
        id="search"
        d="m 13.435357,12.062946 h -0.722722 l -0.256252,-0.24703 A 5.918861,5.918861 0 0 0 13.892642,7.9462471 5.9463053,5.9463053 0 1 0 7.9463873,13.892548 c 1.472806,0 2.8267837,-0.53974 3.8696307,-1.436259 l 0.246803,0.256139 v 0.722702 l 4.57406,4.564932 1.363087,-1.363075 z m -5.4888947,0 c -2.277883,0 -4.116661,-1.838782 -4.116661,-4.1166729 0,-2.277895 1.838778,-4.116672 4.116661,-4.116672 2.2779217,0 4.1166987,1.838777 4.1166987,4.116672 0,2.2778909 -1.838777,4.1166729 -4.1166987,4.1166729 z" />
   </svg>

    )
  }
}