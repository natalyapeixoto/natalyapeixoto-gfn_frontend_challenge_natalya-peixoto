import React from 'react'
import ReactDOM from 'react-dom'

import Container from './components/Container'


class App extends React.Component {

  render(){
    return(
      <React.Fragment>
       <Container />
       </React.Fragment>
    )
  }


}


ReactDOM.render(<App/>, document.getElementById('root'))