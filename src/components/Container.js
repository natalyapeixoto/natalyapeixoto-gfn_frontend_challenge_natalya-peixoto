import React from 'react'

import  './Container.css'

import MainContent from './MainContent.js'




class Container extends React.Component {
  
  render(){
    return(
      <div>
        <header className="header">
          <h1 className="header__title">Desempenho das Lojas</h1>
        </header>
        <MainContent />
      </div>
    )
  }
}

export default Container